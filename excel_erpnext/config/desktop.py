# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Excel ERPNext",
			"color": "grey",
			"icon": "fa fa-cloud",
			"type": "module",
			"label": _("Excel ERPNext")
		}
	]
